<?php

namespace App\Form;

use App\Entity\CurriculumVitae;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CurriculumVitaeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('statusCv')
            
            ->add('dateMaj',DateType::class, [
                'widget' => 'single_text',
                'input'  => 'datetime_immutable'
                    ])
            ->add('versionCv', ChoiceType::class, [
                'choices'  => [
                    'Fr' => 'Fr',
                    'En' => 'En',
                ],
                    ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CurriculumVitae::class,
        ]);
    }
}
