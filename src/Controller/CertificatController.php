<?php

namespace App\Controller;

/* use App\Repository\CertificatRepository; */

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CertificatController extends AbstractController
{

    /**
     * @Route("/cert", name="app_certificat", methods={"GET"})
     */
    public function index(/* CertificatRepository $certificatRepository */): Response
    {
        return $this->render('certificat/index.html.twig', [
            /*  'certificats' => $certificatRepository->findAll(), */]);
    }
}
