<?php

namespace App\Controller;

use App\Entity\CurriculumVitae;
use App\Form\CurriculumVitaeType;
use App\Repository\CurriculumVitaeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CurriculumVitaeController extends AbstractController
{


    /**
     * @Route("/cv", name="app_cv_list")
     */
    public function new(Request $request): Response
    {
        $CurriculumVitae = new CurriculumVitae();
        /*  $collab=new Collaborateur(); */
        $form = $this->createForm(CurriculumVitaeType::class, $CurriculumVitae);
        /* $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $CurriculumVitaeRepository->add($CurriculumVitae, true);

            return $this->redirectToRoute('app_CurriculumVitae_index', [], Response::HTTP_SEE_OTHER);
        }
 */
        return $this->renderForm('curriculumvitae/listeCv.html.twig', [
            /*  'CurriculumVitae' => $CurriculumVitae, */
            'form' => $form,
            /*  'CurriculumVitaes' => $CurriculumVitaeRepository->findAll(), */
        ]);
    }
}
