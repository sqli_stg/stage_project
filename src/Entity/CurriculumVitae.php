<?php

namespace App\Entity;

use App\Repository\CurriculumVitaeRepository;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass=CurriculumVitaeRepository::class)
 */
class CurriculumVitae
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statusCv;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomCurriculumVitae;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateMaj;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $versionCv;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=Collaborateur::class, inversedBy="curriculumVitaes")
     */
    private $AvoirCV;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatusCv(): ?string
    {
        return $this->statusCv;
    }

    public function setStatusCv(string $statusCv): self
    {
        $this->statusCv = $statusCv;

        return $this;
    }

    public function getNomCurriculumVitae(): ?string
    {
        return $this->nomCurriculumVitae;
    }

    public function setNomCurriculumVitae(string $nomCurriculumVitae): self
    {
        $this->nomCurriculumVitae = $nomCurriculumVitae;

        return $this;
    }

    public function getDateMaj(): ?\DateTimeInterface
    {
        return $this->dateMaj;
    }

    public function setDateMaj(\DateTimeInterface $dateMaj): self
    {
        $this->dateMaj = $dateMaj;

        return $this;
    }

    public function getVersionCv(): ?string
    {
        return $this->versionCv;
    }

    public function setVersionCv(string $versionCv): self
    {
        $this->versionCv = $versionCv;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getAvoirCV(): ?Collaborateur
    {
        return $this->AvoirCV;
    }

    public function setAvoirCV(?Collaborateur $AvoirCV): self
    {
        $this->AvoirCV = $AvoirCV;

        return $this;
    }
}
