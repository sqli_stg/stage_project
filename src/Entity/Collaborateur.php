<?php

namespace App\Entity;

use App\Repository\CollaborateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CollaborateurRepository::class)
 */
class Collaborateur
{
     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $Nom;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $prenom;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $email;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     */
    private $dateEntree;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     */
    private $dateSortie;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $profilType;

    /**
     * @ORM\OneToMany(targetEntity=Certificat::class, mappedBy="Obtenir")
     */
    private $certificats;

    /**
     * @ORM\OneToMany(targetEntity=CurriculumVitae::class, mappedBy="AvoirCV")
     */
    private $curriculumVitaes;

    /**
     * @ORM\OneToMany(targetEntity=Mission::class, mappedBy="EffectuerMission")
     */
    private $missionsEffectuee;

    public function __construct()
    {
        $this->certificats = new ArrayCollection();
        $this->curriculumVitaes = new ArrayCollection();
        $this->missionsEffectuee = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateEntree(): ?\DateTimeInterface
    {
        return $this->dateEntree;
    }

    public function setDateEntree(\DateTimeInterface $dateEntree): self
    {
        $this->dateEntree = $dateEntree;

        return $this;
    }

    public function getDateSortie(): ?\DateTimeInterface
    {
        return $this->dateSortie;
    }

    public function setDateSortie(\DateTimeInterface $dateSortie): self
    {
        $this->dateSortie = $dateSortie;

        return $this;
    }

    public function getProfilType(): ?string
    {
        return $this->profilType;
    }

    public function setProfilType(string $profilType): self
    {
        $this->profilType = $profilType;

        return $this;
    }

    /**
     * @return Collection<int, Certificat>
     */
    public function getCertificats(): Collection
    {
        return $this->certificats;
    }

    public function addCertificat(Certificat $certificat): self
    {
        if (!$this->certificats->contains($certificat)) {
            $this->certificats[] = $certificat;
            $certificat->setObtenir($this);
        }

        return $this;
    }

    public function removeCertificat(Certificat $certificat): self
    {
        if ($this->certificats->removeElement($certificat)) {
            // set the owning side to null (unless already changed)
            if ($certificat->getObtenir() === $this) {
                $certificat->setObtenir(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CurriculumVitae>
     */
    public function getCurriculumVitaes(): Collection
    {
        return $this->curriculumVitaes;
    }

    public function addCurriculumVitae(CurriculumVitae $curriculumVitae): self
    {
        if (!$this->curriculumVitaes->contains($curriculumVitae)) {
            $this->curriculumVitaes[] = $curriculumVitae;
            $curriculumVitae->setAvoirCV($this);
        }

        return $this;
    }

    public function removeCurriculumVitae(CurriculumVitae $curriculumVitae): self
    {
        if ($this->curriculumVitaes->removeElement($curriculumVitae)) {
            // set the owning side to null (unless already changed)
            if ($curriculumVitae->getAvoirCV() === $this) {
                $curriculumVitae->setAvoirCV(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Mission>
     */
    public function getMissionsEffectuee(): Collection
    {
        return $this->missionsEffectuee;
    }

    public function addMissionsEffectuee(Mission $missionsEffectuee): self
    {
        if (!$this->missionsEffectuee->contains($missionsEffectuee)) {
            $this->missionsEffectuee[] = $missionsEffectuee;
            $missionsEffectuee->setEffectuerMission($this);
        }

        return $this;
    }

    public function removeMissionsEffectuee(Mission $missionsEffectuee): self
    {
        if ($this->missionsEffectuee->removeElement($missionsEffectuee)) {
            // set the owning side to null (unless already changed)
            if ($missionsEffectuee->getEffectuerMission() === $this) {
                $missionsEffectuee->setEffectuerMission(null);
            }
        }

        return $this;
    }
}
