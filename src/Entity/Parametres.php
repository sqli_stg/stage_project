<?php

namespace App\Entity;

use App\Repository\ParametresRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParametresRepository::class)
 */
class Parametres
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $EmailDenvoi;

    /**
     * @ORM\Column(type="integer")
     */
    private $DelaiDeNotification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmailDenvoi(): ?string
    {
        return $this->EmailDenvoi;
    }

    public function setEmailDenvoi(string $EmailDenvoi): self
    {
        $this->EmailDenvoi = $EmailDenvoi;

        return $this;
    }

    public function getDelaiDeNotification(): ?int
    {
        return $this->DelaiDeNotification;
    }

    public function setDelaiDeNotification(int $DelaiDeNotification): self
    {
        $this->DelaiDeNotification = $DelaiDeNotification;

        return $this;
    }
}
