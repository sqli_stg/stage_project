<?php

namespace App\Entity;

use App\Repository\MissionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MissionRepository::class)
 */
class Mission
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomMission;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateDebutMission;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateFinMission;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $DescriptionMission;

    /**
     * @ORM\ManyToOne(targetEntity=Collaborateur::class, inversedBy="missionsEffectuee")
     */
    private $EffectuerMission;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomMission(): ?string
    {
        return $this->nomMission;
    }

    public function setNomMission(string $nomMission): self
    {
        $this->nomMission = $nomMission;

        return $this;
    }

    public function getDateDebutMission(): ?\DateTimeInterface
    {
        return $this->DateDebutMission;
    }

    public function setDateDebutMission(?\DateTimeInterface $DateDebutMission): self
    {
        $this->DateDebutMission = $DateDebutMission;

        return $this;
    }

    public function getDateFinMission(): ?\DateTimeInterface
    {
        return $this->DateFinMission;
    }

    public function setDateFinMission(?\DateTimeInterface $DateFinMission): self
    {
        $this->DateFinMission = $DateFinMission;

        return $this;
    }

    public function getDescriptionMission(): ?string
    {
        return $this->DescriptionMission;
    }

    public function setDescriptionMission(?string $DescriptionMission): self
    {
        $this->DescriptionMission = $DescriptionMission;

        return $this;
    }

    public function getEffectuerMission(): ?Collaborateur
    {
        return $this->EffectuerMission;
    }

    public function setEffectuerMission(?Collaborateur $EffectuerMission): self
    {
        $this->EffectuerMission = $EffectuerMission;

        return $this;
    }
}
