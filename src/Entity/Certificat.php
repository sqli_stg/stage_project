<?php

namespace App\Entity;

use App\Repository\CertificatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CertificatRepository::class)
 */
class Certificat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NomCertif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateCertif;

    /**
     * @ORM\ManyToOne(targetEntity=Collaborateur::class, inversedBy="certificats")
     */
    private $Obtenir;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCertif(): ?string
    {
        return $this->NomCertif;
    }

    public function setNomCertif(?string $NomCertif): self
    {
        $this->NomCertif = $NomCertif;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getDateCertif(): ?\DateTimeInterface
    {
        return $this->DateCertif;
    }

    public function setDateCertif(?\DateTimeInterface $DateCertif): self
    {
        $this->DateCertif = $DateCertif;

        return $this;
    }

    public function getObtenir(): ?Collaborateur
    {
        return $this->Obtenir;
    }

    public function setObtenir(?Collaborateur $Obtenir): self
    {
        $this->Obtenir = $Obtenir;

        return $this;
    }
}
