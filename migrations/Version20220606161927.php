<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220606161927 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE collaborateur ADD nom VARCHAR(255) DEFAULT NULL, ADD prenom VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD date_entree DATE DEFAULT NULL, ADD date_sortie DATE DEFAULT NULL, ADD profil_type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE curriculum_vitae ADD status_cv VARCHAR(255) DEFAULT NULL, ADD nom_curriculum_vitae VARCHAR(255) DEFAULT NULL, ADD date_maj DATE DEFAULT NULL, ADD version_cv VARCHAR(255) DEFAULT NULL, ADD commentaire VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE collaborateur DROP nom, DROP prenom, DROP email, DROP date_entree, DROP date_sortie, DROP profil_type');
        $this->addSql('ALTER TABLE curriculum_vitae DROP status_cv, DROP nom_curriculum_vitae, DROP date_maj, DROP version_cv, DROP commentaire');
    }
}
