<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220606163615 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mission (id INT AUTO_INCREMENT NOT NULL, effectuer_mission_id INT DEFAULT NULL, nom_mission VARCHAR(255) NOT NULL, date_debut_mission DATE DEFAULT NULL, date_fin_mission DATE DEFAULT NULL, description_mission VARCHAR(255) DEFAULT NULL, INDEX IDX_9067F23CC9F2747 (effectuer_mission_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parametres (id INT AUTO_INCREMENT NOT NULL, email_denvoi VARCHAR(255) NOT NULL, delai_de_notification INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23CC9F2747 FOREIGN KEY (effectuer_mission_id) REFERENCES collaborateur (id)');
        $this->addSql('ALTER TABLE certificat ADD obtenir_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE certificat ADD CONSTRAINT FK_27448F77BE8D6DA2 FOREIGN KEY (obtenir_id) REFERENCES collaborateur (id)');
        $this->addSql('CREATE INDEX IDX_27448F77BE8D6DA2 ON certificat (obtenir_id)');
        $this->addSql('ALTER TABLE curriculum_vitae ADD avoir_cv_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE curriculum_vitae ADD CONSTRAINT FK_1FC99844C21D6DF1 FOREIGN KEY (avoir_cv_id) REFERENCES collaborateur (id)');
        $this->addSql('CREATE INDEX IDX_1FC99844C21D6DF1 ON curriculum_vitae (avoir_cv_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE mission');
        $this->addSql('DROP TABLE parametres');
        $this->addSql('ALTER TABLE certificat DROP FOREIGN KEY FK_27448F77BE8D6DA2');
        $this->addSql('DROP INDEX IDX_27448F77BE8D6DA2 ON certificat');
        $this->addSql('ALTER TABLE certificat DROP obtenir_id');
        $this->addSql('ALTER TABLE curriculum_vitae DROP FOREIGN KEY FK_1FC99844C21D6DF1');
        $this->addSql('DROP INDEX IDX_1FC99844C21D6DF1 ON curriculum_vitae');
        $this->addSql('ALTER TABLE curriculum_vitae DROP avoir_cv_id');
    }
}
